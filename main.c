#include "timer.h"
#include "spi.h"
#include "uart.h"
#include "decode.h"

char cMessageTransimtterd[16];
char cMessageRecieved[16];
extern unsigned char ucTokenNr;
extern struct Token asToken[MAX_TOKEN_NR];
unsigned char ucfPortSet = 0;
unsigned char ucfSPortGet = 0;
unsigned char ucfDacSet = 0;
unsigned char ucfUnknownCommand = 0;

int main(){
	
	UART_InitWithInt(9600);
	Transmiter_SendString(cMessageTransimtterd);
	
	while(1){
		if(eReciever_GetStatus() == READY){
			Reciever_GetStringCopy(cMessageRecieved);
			DecodeMsg(cMessageRecieved);
			if(ucTokenNr > 0 && asToken[0].eType == KEYWORD){
				switch(asToken[0].uValue.eKeyword){
					case PORTSET: 
						ucfPortSet = 1;
						break;
					case MCP_PORT_RD:
						ucfSPortGet = 1;
						break;
					case DACSET:
							ucfDacSet = 1;
						break;
					default:
							ucfUnknownCommand = 1;
						break;
				}
			}
			else{
					ucfUnknownCommand = 1;
			}
		}
		if(Transmiter_GetStatus() == FREE){
			if(ucfPortSet == 1){
				ucfPortSet = 0;
				Port_MCP23S09_Set(asToken[1].uValue.uiNumber);
			}
			else if(ucfSPortGet == 1){
				ucfSPortGet = 0;
				CopyString("portget" , cMessageTransimtterd);
				AppendUIntToString( Port_MCP23S09_Get(), cMessageTransimtterd );
				AppendString("\n" , cMessageTransimtterd);
				Transmiter_SendString(cMessageTransimtterd);
			}
			else if(ucfDacSet == 1){
				ucfDacSet = 0;
				DAC_MCP4921_Set_Adv(asToken[1].uValue.uiNumber);
			}
			else if(ucfUnknownCommand == 1){
				ucfUnknownCommand = 0;
				CopyString("unkonowncommand" , cMessageTransimtterd);
				AppendString("\n" , cMessageTransimtterd);
				Transmiter_SendString(cMessageTransimtterd);
			}
		}
	}
}
