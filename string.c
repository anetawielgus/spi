#include "string.h"

enum CompResult eCompareString(char pcStr1[], char pcStr2[]){
	unsigned char ucLicznikZnakow;
	for (ucLicznikZnakow=0; pcStr1[ucLicznikZnakow] == pcStr2[ucLicznikZnakow]; ucLicznikZnakow++){
		if (NULL == pcStr1[ucLicznikZnakow]){
		return EQUAL;
		}
	}
	return DIFFERENT;
}
enum ReturnValue eHexStringToUInt(char pcStr[],unsigned int *puiValue){
	unsigned char ucCharacterCounter, ucCurrentHexChar;
	unsigned int uiCurrentIntValue = 0;
	if((pcStr[0] != '0') || (pcStr[1] != 'x') || (pcStr[2] == NULL)){
		return ERROR;
	}
	for(ucCharacterCounter = 2; (ucCurrentHexChar = pcStr[ucCharacterCounter]) != NULL; ucCharacterCounter++){
		if(ucCharacterCounter > 6){
			return ERROR;
		}

		uiCurrentIntValue = uiCurrentIntValue << 4;
		if(ucCurrentHexChar >= '0' && ucCurrentHexChar <= '9'){
			uiCurrentIntValue = uiCurrentIntValue | (ucCurrentHexChar - '0');
		}
		else if(ucCurrentHexChar >= 'A' && ucCurrentHexChar <= 'F'){
			uiCurrentIntValue = uiCurrentIntValue | (ucCurrentHexChar - 'A' + 10);
		}
		else{
			return ERROR;
		}
	}
	*puiValue = uiCurrentIntValue;
	return OK;
}

void ReplaceCharactersInString(char acString[],char cOldChar,char cNewChar){
	unsigned char ucCharacterCounter;
	for(ucCharacterCounter = 0; NULL != acString[ucCharacterCounter]; ucCharacterCounter++){
		if(acString[ucCharacterCounter] == cOldChar){
		acString[ucCharacterCounter] = cNewChar;
		}
	}
}

void CopyString (char pcSource[],char pcDestination[]){

    unsigned char ucLicznik;

    for (ucLicznik = 0; pcSource[ucLicznik] != NULL; ucLicznik++){
        pcDestination[ucLicznik] = pcSource[ucLicznik];
    }
    pcDestination[ucLicznik] = pcSource[ucLicznik];
}

void UIntToHexStr( unsigned int uiValue, char pcStr[] ){

    unsigned char ucLicznikZnakow;
    unsigned char ucZnakHex;

    pcStr[0] = '0';
    pcStr[1] = 'x';
    pcStr[6] = NULL;

    for( ucLicznikZnakow = 0; ucLicznikZnakow < 4; ucLicznikZnakow++ ){
        ucZnakHex = ( uiValue >> (ucLicznikZnakow*4) ) & (0x000F);

        if ( ucZnakHex < 10 ){
            pcStr[ 5-ucLicznikZnakow ] = '0' + ucZnakHex;
        }
        else{
            pcStr[ 5-ucLicznikZnakow ] = 'A' + ucZnakHex-10;
        }
    }
}


void AppendUIntToString(unsigned int uiValue, char pcDestinationStr[]){

    unsigned char ucDlugoscNapisu;

    for(ucDlugoscNapisu = 0; pcDestinationStr[ucDlugoscNapisu] != NULL; ucDlugoscNapisu++){}
    UIntToHexStr(uiValue, pcDestinationStr + ucDlugoscNapisu);
}

void AppendString (char pcSourceStr[], char pcDestinationStr[]){

    unsigned char ucLicznik;

    for (ucLicznik=0; pcDestinationStr[ucLicznik] != NULL; ucLicznik++){};
  
  CopyString(pcSourceStr,pcDestinationStr + ucLicznik);
    
}

