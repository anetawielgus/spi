#include <LPC21xx.H>
#include "spi.h"

#define SPI_CLOCK_DIVIDER 0x08

#define SCK_bm  1<<8
#define MISO_bm 1<<10
#define MOSI_bm 1<<12
#define SSEL_bm 1<<14

#define CS_DAC_bm   1<<10

#define CPHA_bm 1<<3
#define CPOL_bm 1<<4
#define MSTR_bm 1<<5
#define LSBF_bm 1<<6

#define DAC_SHDN 1<<4
#define DAC_GAIN 1<<5
#define DAC_BUFF 1<<6
#define DAC_A_CHANNEL 1<<7

#define SPIF_bm 1<<7

#define CS_IO_bm 1<<11
#define WRITE_OPCODE 0x40
#define READ_OPCODE 0x41
#define DIRECTION_REGISTER_ADDR 0
#define DIRECTION_OUTPUT 0
#define DIRECTION_INPUT 1
#define PORT_REGISTER_ADDR 9

void SPI_ConfigMaster(struct SPI_FrameParams sSPI_FrameParams){
	PINSEL0 = PINSEL0 | SCK_bm | MISO_bm | MOSI_bm | SSEL_bm;
	S0SPCR =  (sSPI_FrameParams.ucCpha<<3) | (sSPI_FrameParams.ucCpol<<4) | (sSPI_FrameParams.ucClsbf<<6) | MSTR_bm;;
	S0SPCCR = sSPI_FrameParams.ClkDivider;
}

void SPI_ExecuteTransaction(struct SPI_TransactionParams sSPI_TransactionParams){
	
	
	unsigned char ucNrOfMaxTransactionRxBytes=0;
	unsigned char ucNrOfMaxTransactionTxBytes=0;
	unsigned char ucNrOfMaxTransactionBytes=0;
	
	unsigned char ucBytesRecievedCounter=0;
	unsigned char ucBytesToTransmittCounter=0;
	unsigned char ucTransactionCounter=0;
	
	unsigned char ucBytesRecieved=0;
	unsigned char ucBytesToTransmitt=0;
	
	ucNrOfMaxTransactionRxBytes = sSPI_TransactionParams.ucNrOfBytesForRx + sSPI_TransactionParams.ucRxBytesOffset;
	ucNrOfMaxTransactionTxBytes = sSPI_TransactionParams.ucNrOfBytesForTx + sSPI_TransactionParams.ucTxBytesOffset;
	
	if(ucNrOfMaxTransactionRxBytes>ucNrOfMaxTransactionTxBytes){
		ucNrOfMaxTransactionBytes=ucNrOfMaxTransactionRxBytes;
	}
	else{
		ucNrOfMaxTransactionBytes=ucNrOfMaxTransactionTxBytes;
	}
	
	for(ucTransactionCounter =0; ucTransactionCounter < ucNrOfMaxTransactionBytes ; ucTransactionCounter++){
		if((ucTransactionCounter>=sSPI_TransactionParams.ucTxBytesOffset) & (ucBytesToTransmittCounter<sSPI_TransactionParams.ucNrOfBytesForTx)){
			ucBytesToTransmitt = *(sSPI_TransactionParams.pucBytesForTx+ucBytesToTransmittCounter);
			ucBytesToTransmittCounter++;
		}
		else{
			ucBytesToTransmitt = 0;
		}
		
		S0SPDR = ucBytesToTransmitt;
		while((S0SPSR & SPIF_bm) == 0){}
		ucBytesRecieved = S0SPDR;
		
		if((ucTransactionCounter>=sSPI_TransactionParams.ucRxBytesOffset) & (ucBytesRecievedCounter<sSPI_TransactionParams.ucNrOfBytesForRx)){
			*(sSPI_TransactionParams.pucBytesForRx+ucBytesRecievedCounter) = ucBytesRecieved;
			ucBytesRecievedCounter++;
		}
	}
}

void DAC_MCP4921_InitCSPin(){
	IO0DIR = IO0DIR | CS_DAC_bm;
	IO0SET = CS_DAC_bm;
}

void DAC_MCP4921_Set_Adv(unsigned int uiData){
	struct SPI_TransactionParams MySPI_TransactionParams;
	struct SPI_FrameParams MySPI_FrameParams = {0,0,0,8};

	unsigned char ucBytesForTx[2];
	unsigned char ucBytesForRx[1];
	
	MySPI_TransactionParams.pucBytesForTx = ucBytesForTx;
	MySPI_TransactionParams.ucNrOfBytesForTx = 2;
	MySPI_TransactionParams.ucTxBytesOffset = 0;
	MySPI_TransactionParams.pucBytesForRx = ucBytesForRx;
	MySPI_TransactionParams.ucNrOfBytesForRx = 0;
	MySPI_TransactionParams.ucRxBytesOffset = 0;	
	
	uiData = 4096*uiData/3300;
	SPI_ConfigMaster(MySPI_FrameParams);
	DAC_MCP4921_InitCSPin();
	
	ucBytesForTx[0] = DAC_GAIN | DAC_SHDN | (uiData>>8) ;
	ucBytesForTx[1] = uiData ;
	
	IO0CLR = CS_DAC_bm;
	SPI_ExecuteTransaction(MySPI_TransactionParams);
	IO0SET = CS_DAC_bm;
}

void Port_MCP23S09_InitCSPin(){
	IO0DIR = IO0DIR | CS_IO_bm;
	IO0SET = CS_IO_bm;
}

void Port_MCP23S09_Set(unsigned char ucData){
	struct SPI_FrameParams MySPI_FrameParams = {0,0,0,8};
	struct SPI_TransactionParams MySPI_TransactionParams;
	unsigned char ucBytesForTx[3];
	unsigned char ucBytesForRx[1];
	
	MySPI_TransactionParams.pucBytesForTx = ucBytesForTx; 
	MySPI_TransactionParams.ucNrOfBytesForTx = 3;
	MySPI_TransactionParams.ucTxBytesOffset = 0;
	MySPI_TransactionParams.pucBytesForRx = ucBytesForRx;
	MySPI_TransactionParams.ucNrOfBytesForRx = 0;
	MySPI_TransactionParams.ucRxBytesOffset = 0;

	ucBytesForTx[0] = WRITE_OPCODE;
	ucBytesForTx[1] = DIRECTION_REGISTER_ADDR;
	ucBytesForTx[2] = DIRECTION_OUTPUT;

	SPI_ConfigMaster(MySPI_FrameParams);
	Port_MCP23S09_InitCSPin();

	IO0CLR = CS_IO_bm;
	SPI_ExecuteTransaction(MySPI_TransactionParams);
	IO0SET = CS_IO_bm;
	
	ucBytesForTx[0] = WRITE_OPCODE;
	ucBytesForTx[1] = PORT_REGISTER_ADDR;
	ucBytesForTx[2] = ucData;
	
	IO0CLR = CS_IO_bm;
	SPI_ExecuteTransaction(MySPI_TransactionParams);
	IO0SET = CS_IO_bm;
}

unsigned char Port_MCP23S09_Get(void){
	struct SPI_FrameParams MySPI_FrameParams = {0,0,0,8};
	struct SPI_TransactionParams MySPI_TransactionParams;
	unsigned char ucBytesForTx[3];
	unsigned char ucBytesForRx[3];
	
	MySPI_TransactionParams.pucBytesForTx = ucBytesForTx; 
	MySPI_TransactionParams.ucNrOfBytesForTx = 3;
	MySPI_TransactionParams.ucTxBytesOffset = 0;
	MySPI_TransactionParams.pucBytesForRx = ucBytesForRx;
	MySPI_TransactionParams.ucNrOfBytesForRx = 0;
	MySPI_TransactionParams.ucRxBytesOffset = 0;

	ucBytesForTx[0] = WRITE_OPCODE;
	ucBytesForTx[1] = DIRECTION_REGISTER_ADDR;
	ucBytesForTx[2] = DIRECTION_INPUT;

	SPI_ConfigMaster(MySPI_FrameParams);
	Port_MCP23S09_InitCSPin();

	IO0CLR = CS_IO_bm;
	SPI_ExecuteTransaction(MySPI_TransactionParams);
	IO0SET = CS_IO_bm;
	
	MySPI_TransactionParams.pucBytesForTx = ucBytesForTx; 
	MySPI_TransactionParams.ucNrOfBytesForTx = 2;
	MySPI_TransactionParams.ucTxBytesOffset = 0;
	MySPI_TransactionParams.pucBytesForRx = ucBytesForRx;
	MySPI_TransactionParams.ucNrOfBytesForRx = 1;
	MySPI_TransactionParams.ucRxBytesOffset = 2;
	
	ucBytesForTx[0] = READ_OPCODE;
	ucBytesForTx[1] = PORT_REGISTER_ADDR;
	
	IO0CLR = CS_IO_bm;
	SPI_ExecuteTransaction(MySPI_TransactionParams);
	IO0SET = CS_IO_bm;
	
	return ucBytesForRx[0];
}

void DAC_MCP4921_Set_MiliVoltage(unsigned int uiVoltage){
	uiVoltage = 4096*uiVoltage/3300;
	
	PINSEL0 = PINSEL0 | SCK_bm | MISO_bm | MOSI_bm | SSEL_bm;
	
	IO0DIR = IO0DIR | CS_DAC_bm;
	
	S0SPCR = S0SPCR | MSTR_bm;
	S0SPCR = S0SPCR & (~(CPHA_bm)) & (~(CPOL_bm)) & (~(LSBF_bm));
	S0SPCCR = SPI_CLOCK_DIVIDER;
	
	IO0SET = CS_DAC_bm;
	IO0CLR = CS_DAC_bm;
	
	S0SPDR = DAC_GAIN | DAC_SHDN | (uiVoltage>>8) ;
	while((S0SPSR & SPIF_bm) == 0){}
	S0SPDR = uiVoltage;
	while((S0SPSR & SPIF_bm) == 0){}
		
	IO0SET = CS_DAC_bm;
}

