struct SPI_FrameParams{
	unsigned char ucCpha;
	unsigned char ucCpol;
	unsigned char ucClsbf;
	unsigned char ClkDivider;
};

struct SPI_TransactionParams{
	unsigned char *pucBytesForTx; 																// wskaznik na tablice z bajtami do wyslania
	unsigned char ucNrOfBytesForTx; 															// ilosc bajt�w do wyslania
	unsigned char ucTxBytesOffset; 																// offset bajt�w do wyslania
	
	unsigned char *pucBytesForRx; 																// wskaznik na tablice na odebrane bajty
	unsigned char ucNrOfBytesForRx; 															// ilosc bajt�w do odebrania
	unsigned char ucRxBytesOffset; 																// offset bajt�w do odebrania
};

void DAC_MCP4921_Set_MiliVoltage(unsigned int uiVoltage);
void SPI_ConfigMaster(struct SPI_FrameParams);
void SPI_ExecuteTransaction(struct SPI_TransactionParams);
void DAC_MCP4921_InitCSPin(void);
void DAC_MCP4921_Set_Adv(unsigned int uiData);
void Port_MCP23S09_InitCSPin(void);
void Port_MCP23S09_Set(unsigned char ucData);
unsigned char Port_MCP23S09_Get(void);

