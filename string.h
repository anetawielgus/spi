#define NULL 0
enum ReturnValue {ERROR, OK};
enum CompResult {DIFFERENT, EQUAL};

enum CompResult eCompareString(char pcStr1[], char pcStr2[]);
enum ReturnValue eHexStringToUInt(char pcStr[],unsigned int *puiValue);
void ReplaceCharactersInString(char acString[],char cOldChar,char cNewChar);
void CopyString (char pcSource[],char pcDestination[]);
void AppendUIntToString(unsigned int uiValue, char pcDestinationStr[]);
void UIntToHexStr( unsigned int uiValue, char pcStr[] );
void AppendString (char pcSourceStr[], char pcDestinationStr[]);
